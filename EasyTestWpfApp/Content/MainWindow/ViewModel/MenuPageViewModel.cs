﻿using EasyTestWpfApp.Content.MainWindow.View.Pages;
using EasyTestWpfApp.Helpers;
using EasyTestWpfApp.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace EasyTestWpfApp.Content.MainWindow.ViewModel
{
    public class MenuPageViewModel : ViewModelBase
    {
        public MenuPageViewModel()
        {
            GoToPage = new RelayCommand<MainMenu>(ShowPage);
        }

       

        public RelayCommand<MainMenu> GoToPage { get; private set; }

        private void ShowPage(MainMenu menu)
        {
      


            Switcher.Switch(new QuizPageView());
        }
    }
}
