namespace Model.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UsersInfo")]
    public partial class UsersInfo
    {
        public UsersInfo()
        {
            QuizQuestions = new HashSet<QuizQuestion>();
            UsersAnswers = new HashSet<UsersAnswer>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }

        [Required]
        [StringLength(255)]
        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        public string MidleName { get; set; }

        public int CountryId { get; set; }

        public int UniversityId { get; set; }

        public int DepartamentId { get; set; }

        public int CathedraId { get; set; }

        public int Course { get; set; }

        [StringLength(10)]
        public string Groups { get; set; }

        public DateTime Datetime { get; set; }

        public virtual ICollection<QuizQuestion> QuizQuestions { get; set; }

        public virtual ICollection<UsersAnswer> UsersAnswers { get; set; }
    }
}
