﻿using EasyTest.BL.Entity;
using EasyTest.BL.Helpers;
using EasyTest.BL.Quiz.Model;
using Fiddler;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace EasyTest.BL.Quiz
{
    public class MoodleQuiz : Quiz
    {
        public MoodleQuiz()
        {
            StartTestUrl = "/moodle/mod/quiz/startattempt.php";
            TestPagePattern = @"^\/moodle\/mod\/quiz\/attempt\.php\?attempt=\d+$";
            FinishTestUrl = "/moodle/mod/quiz/processattempt.php";
        }

        protected override void FiddlerApplication_AfterSessionComplete(Session oSession)
        {
            CheckFiddlerSettings(oSession);

            String PatchAndQuery = oSession.PathAndQuery;

            //Start test
            if (String.Compare(PatchAndQuery, StartTestUrl) == 0 && oSession.oRequest.headers.HTTPMethod == "POST")
            {
                Status = QuizStatus.Start;

                var reqBody = Encoding.UTF8.GetString(oSession.RequestBody);

                var QueryParams = HttpUtility.ParseQueryString(reqBody);

                quiztData.tid = Convert.ToInt32(QueryParams["cmid"]);

                quiztData.parameter.Add("sessKey", QueryParams["sesskey"]);

                quiztData.Cookie = oSession.oRequest.headers["Cookie"];

                return;
            }

            //Get test questions
            if (Regex.IsMatch(PatchAndQuery, TestPagePattern, RegexOptions.IgnoreCase))
            {
                if (Status != QuizStatus.Start)
                {
                    log.Warn("Test started before started scan!!!");
                    return;
                }

                Status = QuizStatus.ToDo;

                GetQuestionsFromHtmlQuiz(oSession.GetResponseBodyAsString());

                if (quiztData.nextPage != -1)
                {
                    GetNextPageOfTest();
                }

                return;

            }

            if (String.Compare(PatchAndQuery, FinishTestUrl) == 0 && oSession.oRequest.headers.HTTPMethod == "POST")
            {
                Status = QuizStatus.Finish;
                
                return;
            }
        }


        private async void GetNextPageOfTest()
        {
            using (var handler = new HttpClientHandler { UseCookies = false })
            using (var client = new HttpClient(handler) { BaseAddress = new Uri("http://localhost/") })
            {
                var url = String.Format("moodle/mod/quiz/attempt.php?attempt={0}&page={1}", quiztData.parameter["attempt"], quiztData.nextPage);

                var message = new HttpRequestMessage(HttpMethod.Get, url);

                message.Headers.Add("Cookie", quiztData.Cookie);

                var response = await client.SendAsync(message);

                if (response.IsSuccessStatusCode)
                {
                    GetQuestionsFromHtmlQuiz(await response.Content.ReadAsStringAsync());

                    if (quiztData.nextPage != -1)
                        GetNextPageOfTest();
                }

            }
        }

        private const String CourseNameXPath = "//head/title";

        private const String TestNameXPath = "//*[@id='page-header']/h1";

        public override QuizStatus Status
        {
            get
            {
                return Status;
            }

            set
            {
                Status = value;
                StatusChanged();
            }
        }

        public override event Action StatusChanged;

        protected override void GetQuestionsFromHtmlQuiz(String text)
        {
            if (String.IsNullOrEmpty(text))
            {
                throw new ArgumentException();
            }

            var htmlDoc = new HtmlDocument();

            htmlDoc.LoadHtml(text);

            quiztData.quizName = GetQuiztName(htmlDoc, CourseNameXPath, TestNameXPath);


            quiztData.nextPage = Convert.ToInt16(htmlDoc.DocumentNode
                                                 .SelectSingleNode(".//input[@type='hidden' and @name='nextpage']")
                                                 .Attributes["value"]
                                                 .Value);

            var attempt = htmlDoc.DocumentNode
                                  .SelectSingleNode(".//input[@type='hidden' and @name='attempt']")
                                  .Attributes["value"]
                                  .Value;


            quiztData.parameter.Add("attempt", attempt);

            var questions = htmlDoc.DocumentNode
                                .Descendants("div")
                                .Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Equals("que"));

            if (questions != null)
            {
                foreach (var item in questions)
                {
                    html.AppendLine(item.OuterHtml);
                }
            }
        }






        public override void GetQuizWithAnswers()
        {
            throw new NotImplementedException();
        }

        private MoodleQuizParser parser;

        private List<MoodleQuizQuestion> questions;

        public override void ParseQuiz()
        {
            Status = QuizStatus.ParseStart;

            parser = new MoodleQuizParser();

            parser.ParseHtml(html.ToString());

            questions = parser.GetResult();

            Status = QuizStatus.ParseComplated;
        }

        public async override void SaveUniqueQuestions()
        {
            Status = QuizStatus.SaveUniqueStart;

            var data = new
            {
                tid = quiztData.tid,
                quizName = quiztData.quizName,
                questions = questions
            };

            await HttpHelper.SaveUniqueQuestionsAsync(data,"api/MoodleQuiz/SaveUnique");

            Status = QuizStatus.SaveUniqueComplated;
        }
    }
}
