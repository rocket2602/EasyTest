﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasyTestWpfApp.Helpers
{
    public static class Validation
    {
        public static Boolean IsEmail(String strEmail)
        {
            if (String.IsNullOrEmpty(strEmail)) return false;

            var rgxEmail = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                       @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                       @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

            return rgxEmail.IsMatch(strEmail.Trim());
        }

        public static Boolean Username(String username)
        {
            if (String.IsNullOrEmpty(username)) return false;

            if (!IsEmail(username)) return false;

            return true;
        }

        public static Boolean Password(String password)
        {
            if (String.IsNullOrEmpty(password) || password.Length < 6) return false;//6 - const in this case, password length

            return true;
        }
    }
}
