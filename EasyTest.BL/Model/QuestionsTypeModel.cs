namespace EasyTest.BL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("QuestionsType")]
    public  class QuestionsTypeModel
    {
        public QuestionsTypeModel()
        {
            QuizQuestions = new HashSet<QuizQuestionModel>();
        }

        public int Id { get; set; }


        [Required]
        [StringLength(25)]
        public string Name { get; set; }


        public virtual ICollection<QuizQuestionModel> QuizQuestions { get; set; }
    }
}
