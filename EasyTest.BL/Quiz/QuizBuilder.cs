﻿using Fiddler;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTest.BL.Quiz
{
    public class QuizBuilder
    {
        private readonly Quiz quiz;

        public QuizBuilder(Quiz quiz)
        {
            this.quiz = quiz;
        
            this.quiz.StatusChanged += Quiz_StatusChanged;
        }

        public void GetQuiz()/*1-Start fidler and get quiz html*/
        {         
            quiz.StartScan();
        }

        private void Quiz_StatusChanged()
        {
            Debug.WriteLine(quiz.Status);

            switch (quiz.Status)
            {
                case Quiz.QuizStatus.QuestionsInside: quiz.ParseQuiz(); break;
                case Quiz.QuizStatus.ParseComplated: quiz.SaveUniqueQuestions(); break;
                case Quiz.QuizStatus.SaveUniqueComplated: quiz.GetQuizWithAnswers(); break;                    
            }        
        }       
    }
}
