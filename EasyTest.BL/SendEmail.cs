﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.BL
{
    public static class SendEmail
    {
        private static readonly String from,passwd;


        static  SendEmail()
        {
            //from = HashHelper.Decrypt(ConfigurationManager.AppSettings["EmailAccount"]);
            //passwd = HashHelper.Decrypt(ConfigurationManager.AppSettings["EmailPassword"]);

            from = ConfigurationManager.AppSettings["EmailAccount"];
            passwd = ConfigurationManager.AppSettings["EmailPassword"];

        }


        public static Task SendAsync(String to, String subject, String body)
        {
            try
            {           
                var smtp = new System.Net.Mail.SmtpClient();
               // smtp.Port = 587;
               //smtp.Host = "smtp.gmail.com";

                smtp.Port = 2525;
                smtp.Host = "mail.easytest.com.ua";
               smtp.EnableSsl = false;//true when gmail
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential(from, passwd);
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
               
                var message = new System.Net.Mail.MailMessage();
                
               // message.From = new System.Net.Mail.MailAddress(from);
                message.From = new System.Net.Mail.MailAddress("uh343124@easytest.com.ua");
                message.To.Add(new System.Net.Mail.MailAddress(to));
                message.Subject = subject;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;
                message.Body = body;

                return smtp.SendMailAsync(message);
             
            }
            catch(Exception ex)
            {    //log todo   
                throw new Exception("Error send email. "+ex.Message);
               
            }

        }

    }
}
