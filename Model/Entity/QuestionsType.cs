namespace Model.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("QuestionsType")]
    public partial class QuestionsType
    {
        public QuestionsType()
        {
            QuestionsTypeLangs = new HashSet<QuestionsTypeLang>();
            QuizQuestions = new HashSet<QuizQuestion>();
        }

        public int Id { get; set; }

        public int LMSId { get; set; }

        [Required]
        [StringLength(20)]
        public string qType { get; set; }

        public virtual ICollection<QuestionsTypeLang> QuestionsTypeLangs { get; set; }

        public virtual ICollection<QuizQuestion> QuizQuestions { get; set; }
    }
}
