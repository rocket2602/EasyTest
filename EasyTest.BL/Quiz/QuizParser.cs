﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTest.BL.Quiz
{
    public abstract class QuizParser
    {
        public abstract void ParseHtml(String html);

        public abstract dynamic GetResult();
    }
}
