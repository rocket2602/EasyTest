﻿using EasyTestWpfApp.Content.MainWindow.View;
using EasyTestWpfApp.Content.MainWindow.ViewModel;
using EasyTestWpfApp.Model;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;

namespace EasyTestWpfApp.Content.LoginWindow.View
{
    /// <summary>
    /// Interaction logic for LoginWindowView.xaml
    /// </summary>
    public partial class LoginWindowView : Window
    {
        MainWindowView window;

        public LoginWindowView()
        {
            InitializeComponent();

            Messenger.Default.Register<Token>(this, ShowMainWindow);
        }

        private void ShowMainWindow(Token token)
        {
            //var mainWindowVM = new MainWindowViewModel(token);

            //window.DataContext = mainWindowVM;

            //window.Show();

            //this.Hide();

            //window.Closed += (s, e) => { this.Show(); };
        }
    }
}
