namespace Model.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class QuizQuestion
    {
        public QuizQuestion()
        {
            MoodleMatchSubquestions = new HashSet<MoodleMatchSubquestion>();
            QuizAnswers = new HashSet<QuizAnswer>();
            QuizMultiQuestions = new HashSet<QuizMultiQuestion>();
            UsersAnswers = new HashSet<UsersAnswer>();
        }

        public long Id { get; set; }

        public int QuizId { get; set; }

        public int Type { get; set; }

        [Required]
        public string Name { get; set; }

        public int? UserId { get; set; }

        public long? ParentId { get; set; }

        public long TimeCreated { get; set; }

        public virtual ICollection<MoodleMatchSubquestion> MoodleMatchSubquestions { get; set; }

        public virtual QuestionsType QuestionsType { get; set; }

        public virtual ICollection<QuizAnswer> QuizAnswers { get; set; }

        public virtual ICollection<QuizMultiQuestion> QuizMultiQuestions { get; set; }

        public virtual UsersInfo UsersInfo { get; set; }

        public virtual ICollection<UsersAnswer> UsersAnswers { get; set; }
    }
}
