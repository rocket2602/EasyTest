namespace Model.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class QuestionsTypeLang
    {
        public int Id { get; set; }

        public int LanguageId { get; set; }

        public int QuestionTypeId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public virtual Language Language { get; set; }

        public virtual QuestionsType QuestionsType { get; set; }
    }
}
