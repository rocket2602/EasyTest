﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTest.BL.Quiz.Model
{
    public enum MoodleQuestionType
    {
        truefalse = 1,
        multichoice,
        multianswer,
        essay,
        shortanswer,
        numerical,
        match,
        randomsamatch,
        calculated,
        calculatedmulti,
        calculatedsimple,
        multisubanswers
    }

    public class MoodleQuizQuestion: QuizQuestionModel
    {
        public MoodleQuizQuestion()
        {          
            CalculatedValues = new Dictionary<string, double>();
        }
     
        public Int64 qid { get; set; }

        public MoodleQuestionType Type { get; set; }
          
        public MoodleQuestionType AnswersType { get; set; }

        public Dictionary<String, Double> CalculatedValues { get; set; }
    }
}
