﻿using EasyTest.BL.Quiz.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTest.BL.Quiz
{
    public interface IQuizParser<T> where T : QuizQuestionModel
    {
        List<T> ParseHtml(String html);
    }
}
