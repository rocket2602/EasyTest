﻿using EasyTest.BL.Entity;
using EasyTest.BL.Quiz.Model;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace EasyTest.BL.Helpers
{
    public static  class HttpHelper
    {
        public static String baseUrl = "http://localhost:55422/";//save in strings settings

        public static async Task<long[]> GetUniqueqidAsync(long[] qid)
        {
            long[] unique= { };

            using (var client = new HttpClient() { BaseAddress= new Uri(baseUrl)})
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Your Oauth token");


              String querySring =   GenerateQueryStrings(qid);



              var response = await client.GetAsync("api/AtutorQuiz/GetUnique" + querySring);

                if (response.IsSuccessStatusCode)
                {
                    unique = await response.Content.ReadAsAsync<long[]>();
                }
                else
                {
                    throw new Exception("asfrh");
                }
            }

            return unique;
        }

        private static string GenerateQueryStrings(long[] qid)
        {
            String queryStrings = "?qid=" + String.Join("&qid=", qid);

            return queryStrings;
        }

        internal static async Task<Int32> SaveUniqueQuestionsAsync(object data,String url)
        {
            Int32 tid = -1;

            using (var client = new HttpClient() { BaseAddress = new Uri(baseUrl)})
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Your Oauth token");
                var response = await client.PostAsJsonAsync(url, data);

                if (response.IsSuccessStatusCode)
                {
                    tid = await   response.Content.ReadAsAsync<Int32>();
                }
            }

            return tid;
        }

        internal static async Task<String> GetHtmlFromIframeAsync(String url, Quiz.Quiz.QuizTempRuntimeData data)
        {
            using (var handler = new HttpClientHandler { UseCookies = false })
            using (var client = new HttpClient(handler) )//change todo
            {
                var message = new HttpRequestMessage(HttpMethod.Get, url);

                message.Headers.Add("Cookie", data.Cookie);
                message.Headers.Add("User-Agent", data.parameter["UserAgent"]);

                var response = await client.SendAsync(message);

                if (response.IsSuccessStatusCode)
                {
                    HtmlNode.ElementsFlags.Remove("form");

                    var htmlDoc = new HtmlDocument();

                    String responceHtml = await response.Content.ReadAsStringAsync();

                    htmlDoc.LoadHtml(responceHtml);


                    var html = htmlDoc.DocumentNode.SelectSingleNode("//form[@method='get']");


                    return html != null ? html.InnerHtml : String.Empty;
                }

             //   log.Error("Cannot get question from iframe");

                throw new Exception("Cannot get question from iframe");
            }
        }

        internal static async Task<IEnumerable<Entity.QuizQuestionModel>> GetQuestionsAsync(String url,Int32 tid, long[] qid)
        {
            using (var client = new HttpClient() { BaseAddress = new Uri(baseUrl) })
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Your Oauth token");

               String queryStrings =  GenerateQueryStrings(qid);

               var response = await client.GetAsync(url + queryStrings+"&id="+tid);

                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<IEnumerable<Entity.QuizQuestionModel>>();
                }
            }

            return null;
        }
    }
}
