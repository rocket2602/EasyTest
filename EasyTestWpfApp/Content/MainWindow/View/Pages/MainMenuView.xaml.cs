﻿using System.Windows.Controls;

namespace EasyTestWpfApp.Content.MainWindow.View.Pages
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenuView : UserControl
    {
        public MainMenuView()
        {
            InitializeComponent();
        }
    }
}
