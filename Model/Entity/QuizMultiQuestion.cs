namespace Model.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class QuizMultiQuestion
    {
        public QuizMultiQuestion()
        {
            UsersAnswers = new HashSet<UsersAnswer>();
        }

        public long Id { get; set; }

        public long QuestionId { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual QuizQuestion QuizQuestion { get; set; }

        public virtual ICollection<UsersAnswer> UsersAnswers { get; set; }
    }
}
