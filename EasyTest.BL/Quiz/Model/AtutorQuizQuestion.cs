﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTest.BL.Quiz.Model
{
    public enum AtutorQuestionsType
    {
        truefalse = 1,
        multianswer,
        multichoice,
        //Likers,//Dont use in quiz
        ordering ,
        openEnder,
        matchingSimple,
        matchingGraphical
    };


    public class AtutorQuizQuestion : QuizQuestionModel
    {
        public Int64 qid { get; set; }

        public AtutorQuestionsType Type { get; set; }            
    }
}
