﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTestWpfApp.Helpers
{
    public static class Settings
    {
        public static String GetAppSettingsByKey(String key)
        {
            if (String.IsNullOrEmpty(key)) return String.Empty;

            return ConfigurationManager.AppSettings[key];
        }
    }
}
