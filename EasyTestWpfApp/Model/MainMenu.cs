﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTestWpfApp.Model
{
    public enum MainMenu
    {
        MenuPage,
        QuizPage,
        TestPage,
        UserProfilePage,
        SettingsPage,
        HelpPage,
        AboutPage
    }
}
