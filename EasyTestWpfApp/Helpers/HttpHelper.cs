﻿using EasyTestWpfApp.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyTestWpfApp.Helpers
{
    public static class HttpHelper
    {
        public static async Task<Token> SignInUser(String address, String username, String password)
        {
            using (var client = new HttpClient())
            {
                Token token = null;

                client.BaseAddress = new Uri(address);

                var form = new Dictionary<String, String>
                {
                    {"grant_type","password"},
                    {"username",username},
                    {"password",password}
                };

                var tokenResponse = (await client.PostAsync(client.BaseAddress + "token", new FormUrlEncodedContent(form)));

                if (tokenResponse.IsSuccessStatusCode)
                {
                   // token = (await tokenResponse.Content.ReadAsAsync<Token>(new[] { new JsonMediaTypeFormatter() }));
                }

                return token;
            }
        }
    }
}
