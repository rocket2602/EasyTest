﻿using EasyTest.BL.Quiz.Model;
using Fiddler;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;
using nlog =  NLog;
using EasyTest.BL.Entity;


namespace EasyTest.BL.Quiz
{
    public abstract class Quiz:IDisposable
    {
        #region Fileds
       
        public static nlog.Logger log = nlog.LogManager.GetCurrentClassLogger();

        public String StartTestUrl { get; set; }

        public String TestPagePattern { get; set; }

        public String FinishTestUrl { get; set; }

        public String domain { get; set; }

        protected StringBuilder html = new StringBuilder();

        public virtual IEnumerable<Entity.QuizQuestionModel> Result { get;protected set; }

        #endregion


        public QuizTempRuntimeData quiztData;

        public abstract event Action StatusChanged;


        public abstract QuizStatus Status { get; set; }
     
        protected UrlCaptureConfiguration CaptureConfiguration;

     
        public struct QuizTempRuntimeData
        {
            public Int32 tid { get; set; }

            public String quizName { get; set; }

            public String Cookie { get; set; }

            public Int32 nextPage { get; set; }

            public Dictionary<String, String> parameter;
        }

        public enum QuizStatus : byte
        {
            Start,
            ToDo,
            QuestionsInside,
            ParseStart,
            ParseComplated,
            SaveUniqueStart,
            SaveUniqueComplated,
            GetAnswersStart,
            GetAnswersComplated,       
            Finish
        }

        #region Methods



        public void StartScan()
        {
            //Init
            CaptureConfiguration = new UrlCaptureConfiguration() { CaptureDomain = domain, IgnoreResources = true };

            quiztData = new QuizTempRuntimeData()
            {
                parameter = new Dictionary<string, string>()
            };
            //Start proxy server
            FiddlerApplication.AfterSessionComplete += FiddlerApplication_AfterSessionComplete;

            FiddlerApplication.Startup(8888, true, false, true);
        }

        public void StopScan()
        {
            if (FiddlerApplication.IsStarted()) FiddlerApplication.Shutdown();
        }

        protected void CheckFiddlerSettings(Session oSession)
        {
            // Ignore HTTPS connect requests
            if (oSession.RequestMethod == "CONNECT")
                return;

            if (CaptureConfiguration.ProcessId > 0)
            {
                if (oSession.LocalProcessID != 0 && oSession.LocalProcessID != CaptureConfiguration.ProcessId)
                    return;
            }

            if (!string.IsNullOrEmpty(CaptureConfiguration.CaptureDomain))
            {
                if (oSession.hostname.ToLower() != CaptureConfiguration.CaptureDomain.Trim().ToLower())
                    return;
            }

            if (CaptureConfiguration.IgnoreResources)
            {
                string url = oSession.fullUrl.ToLower();

                var extensions = CaptureConfiguration.ExtensionFilterExclusions;
                foreach (var ext in extensions)
                {
                    if (url.Contains(ext))
                        return;
                }

                var filters = CaptureConfiguration.UrlFilterExclusions;
                foreach (var urlFilter in filters)
                {
                    if (url.Contains(urlFilter))
                        return;
                }
            }

            if (oSession == null || oSession.oRequest == null || oSession.oRequest.headers == null)
                return;
        }

        protected String GetQuiztName(HtmlDocument htmlDoc, String course, String test)
        {
            if (htmlDoc == null)
            {
                throw new ArgumentNullException();
            }

            String courseName, testName;

            var courseElem = htmlDoc.DocumentNode.SelectSingleNode(course);

            var testElem = htmlDoc.DocumentNode.SelectSingleNode(test);

            courseName = courseElem != null ? courseElem.InnerText : String.Empty;

            testName = testElem != null ? testElem.InnerText : String.Empty;

            return String.Format("{0} ({1})", courseName.Trim(), testName);

        }

        #endregion

        #region AbstractMethods    

        public abstract void ParseQuiz();
      

        public abstract void SaveUniqueQuestions();

        public abstract void GetQuizWithAnswers();

        protected abstract void GetQuestionsFromHtmlQuiz(String html);

        protected abstract void FiddlerApplication_AfterSessionComplete(Session oSession);

        public void Dispose()//implementation todo
        {
            this.Dispose();
        }

        #endregion
    }
}
