﻿using EasyTest.BL.Quiz;
using EasyTestWpfApp.Helpers;
using Fiddler;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using EasyTest.BL.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shell;
using System.Threading;

namespace EasyTestWpfApp.Content.MainWindow.ViewModel
{
    public class QuizPageViewModel : ViewModelBase
    {
        public QuizPageViewModel()
        {
            StartScan = new RelayCommand(QuizScanOn);

            StopScan = new RelayCommand(QuizScanOff);

            QuizCollection = new ObservableCollection<QuizQuestionModel>();

            Search = new RelayCommand<string>(EthernetSearch);

            Application.Current.MainWindow.Closing += new CancelEventHandler(MainWindow_Closing);

        }


        private void EthernetSearch(string url)
        {
            Process.Start(new ProcessStartInfo(url));
        }



        void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            Debug.WriteLine("Main window closed");

            if (FiddlerApplication.IsStarted()) FiddlerApplication.Shutdown();
        }

        private Quiz quiz;

        private void QuizScanOff()
        {
            if (quiz != null)
                quiz.StopScan();

        }

        private String _animation = "Hidden";

        public String Animation
        {
            get { return _animation; }
            set
            {
                _animation = value; RaisePropertyChanged("Animation");
            }
        }

        private void QuizScanOn()
        {
            String domain = "http://localhost:8080";

            //get userid
            //get user university [host,lms type]

            try
            {
                quiz = new AtutorQuiz();

                quiz.domain = domain;

                var builder = new QuizBuilder(quiz);

                builder.GetQuiz();

                quiz.StatusChanged += Quiz_StatusChanged;




                /*
                QuizDravInForm(collection);

                */
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public ObservableCollection<QuizQuestionModel> QuizCollection { get; set; }

        private QuizQuestionModel _selectedQuestionModel;

        public QuizQuestionModel SelectedQuestionModel
        {
            get { return _selectedQuestionModel; }
            set { RaisePropertyChanged("SelectedQuestionModel"); }
        }

        private void Quiz_StatusChanged()
        {
            switch (quiz.Status)
            {
                case Quiz.QuizStatus.GetAnswersComplated:

                    var questions = quiz.Result;

                    foreach (var item in questions)
                    {
                        App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
            {

                QuizCollection.Add(item);
            });
                    }

                    break;
            }


        }

        #region Command

        public RelayCommand StartScan { get; private set; }
        public RelayCommand StopScan { get; private set; }

        public RelayCommand<String> Search { get; private set; }


        #endregion

    }
}
