﻿using System.Windows.Controls;
using EasyTestWpfApp.Content.MainWindow.View;

namespace EasyTestWpfApp.Helpers
{
    public static class Switcher
    {
        public static MainWindowView pageSwitcher;

        public static void Switch(UserControl page)
        {
            pageSwitcher.Navigate(page);

        }
    }
}
