﻿using EasyTest.BL.Quiz.Model;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace EasyTest.BL.Quiz
{
    public class AtutorQuizParser : IQuizParser<AtutorQuizQuestion>
    {
        private readonly List<AtutorQuizQuestion> _questionsStorage;

        private AtutorQuizParser()
        {
            _questionsStorage = new List<AtutorQuizQuestion>();
        }

        private static AtutorQuizParser instance;

        public static IQuizParser<AtutorQuizQuestion> Instance
        {
            get
            {
                if (instance == null)
                    instance = new AtutorQuizParser();

                return instance;
            }
        }

        public List<AtutorQuizQuestion> ParseHtml(String html)
        {
            var htmlDoc = new HtmlDocument();

            htmlDoc.LoadHtml(html);

            var questions = htmlDoc.DocumentNode.SelectNodes("//div[@class='row']");

            if (questions == null)
            {
                throw new Exception("Questions not found in the page");
            }

            foreach (var item in questions)
            {
                var ulElem = item.SelectSingleNode(".//ul[not(@class='matching-question')]");

                /* Multianswer|Multichoice|Ordering|MatchingGraphical */
                if (ulElem != null)
                {
                    var questionType = ulElem.Attributes["class"].Value;

                    switch (questionType)
                    {
                        case "multianswer-question": ParseMultiAnswerQuestion(item, ulElem); break;
                        case "multichoice-question": ParseMultiChoiceQuestion(item, ulElem); break;
                        case "ordering-question": ParseOrderingQuestion(item, ulElem); break;
                        case "dd-question": ParseMatchingGraphicalQuestion(item, ulElem); break;
                        default: /*some error*/ break;
                    }
                    continue;
                }
                /* TrueFalse|OpenEnder|MatchingSimple */
                var TrueFalse = item.SelectNodes(".//p/label[not(strong)]");

                if (TrueFalse != null)
                {
                    ParseTrueFalseQuestion(item, TrueFalse);

                    continue;
                }

                var OpenEnder = item.SelectSingleNode(".//input[@class='formfield'] | .//textarea[@class='formfield']");

                if (OpenEnder != null)
                {
                    ParseOpenEnderQuestion(item,OpenEnder);

                    continue;
                }

                var MatchingSimple = item.SelectNodes(".//ul[@class='matching-question']");

                if (MatchingSimple != null)
                {
                    ParseMatchingSimpleQuestion(item, MatchingSimple);

                    continue;
                }
                //someerror
            }
            return _questionsStorage;
        }

        #region ParseQuestions

        private void ParseTrueFalseQuestion(HtmlNode item, HtmlNodeCollection answers)
        {
            var question = new AtutorQuizQuestion()
            {
                Type = AtutorQuestionsType.truefalse,
                Question = GetQuestion(item),
                qid = GetQuestionId(item, ".//input")
            };

            foreach (var answ in answers)
            {
                question.Answers.Add(answ.InnerText.Trim());
            }

            _questionsStorage.Add(question);
        }

        private void ParseMultiChoiceQuestion(HtmlNode item, HtmlNode ul)
        {
            var question = new AtutorQuizQuestion()
            {
                Type = AtutorQuestionsType.multichoice,
                Question = GetQuestion(item),
                qid = GetQuestionId(item, ".//input")
            };

            var answers = ul.SelectNodes("./li/label[not(strong)]");

            foreach (var answ in answers)
            {
                question.Answers.Add(answ.InnerText.Trim());
            }

            _questionsStorage.Add(question);
        }

        private void ParseMultiAnswerQuestion(HtmlNode item, HtmlNode ul)
        {
            var question = new AtutorQuizQuestion()
            {
                Type = AtutorQuestionsType.multianswer,
                Question = GetQuestion(item),
                qid = GetQuestionId(item, ".//input[@type='hidden']")
            };

            var answersNode = ul.SelectNodes(".//li/label");

            foreach (var answ in answersNode)
            {
                question.Answers.Add(answ.InnerText.Trim());
            }

            _questionsStorage.Add(question);
        }

        private void ParseOrderingQuestion(HtmlNode item, HtmlNode ul)
        {
            var question = new AtutorQuizQuestion()
            {
                Type = AtutorQuestionsType.ordering,
                Question = GetQuestion(item),
                qid = GetQuestionId(item, ".//select")
            };

            var answersElem = ul.SelectNodes(".//li/label");

            foreach (var answ in answersElem)
            {
                question.Answers.Add(answ.InnerText.Trim());
            }

            _questionsStorage.Add(question);
        }

        private void ParseOpenEnderQuestion(HtmlNode item,HtmlNode elem)
        {
            var question = new AtutorQuizQuestion()
            {
                Type = AtutorQuestionsType.openEnder,
                Question = GetQuestion(item),
                qid = GetQuestionId(elem)
            };

            _questionsStorage.Add(question);
        }

        private void ParseMatchingSimpleQuestion(HtmlNode item, HtmlNodeCollection elem)
        {
            var question = new AtutorQuizQuestion()
            {
                Type = AtutorQuestionsType.matchingSimple,
                Question = GetQuestion(item),
                qid = GetQuestionId(item, ".//select")
            };

            bool first = false;

            foreach (var item2 in elem)
            {
                var AOrQ = item2.SelectNodes(".//li");

                if (AOrQ != null)
                {
                    if (!first)
                    {
                        foreach (var q in GetMultiQuestions(AOrQ))
                        {
                            question.MultiQuestions.Add(q);
                        }

                        first = true;
                    }
                    else
                    {
                        foreach (var answ in AOrQ)
                        {
                            question.Answers.Add(answ.InnerText.Trim());
                        }

                        ChangeAnswersListTypeToNumber(question.Answers);
                    }
                }
            }
            _questionsStorage.Add(question);
        }

        private void ParseMatchingGraphicalQuestion(HtmlNode item, HtmlNode ul)
        {
            var question = new AtutorQuizQuestion()
            {
                Type = AtutorQuestionsType.matchingGraphical,
                Question = GetQuestion(item),
                qid = GetQuestionId(item, ".//input[@type='hidden']")
            };

            var MultiQuestions = ul.SelectNodes(".//li[@class='question']");

            foreach (var mq in GetMultiQuestions(MultiQuestions))
            {
                question.MultiQuestions.Add(mq);
            }

            var MultiAnswers = item.SelectNodes(".//ol/li[@class='answer']");

            foreach (var answ in MultiAnswers)
            {
                question.Answers.Add(answ.InnerText.Trim());
            }

            ChangeAnswersListTypeToNumber(question.Answers);

            _questionsStorage.Add(question);
        }
        #endregion

        #region Helpers
        private string GetQuestion(HtmlNode item)
        {
            var question = item.SelectSingleNode(".//p");

            if (question == null) return String.Empty;

            return question.InnerHtml;
        }

        private Int64 GetQuestionId(HtmlNode element)
        {
            var value = element.Attributes["name"].Value;

            var pattern = @"answers\[(\d+)\]";

            Match match = Regex.Match(value, pattern);

            if (match.Success)
                return Convert.ToInt64(match.Groups[1].Value);

            return -1;
        }

        private Int64 GetQuestionId(HtmlNode element, String xPath)
        {
            var item = element.SelectNodes(xPath).FirstOrDefault();

            var value = item.Attributes["name"].Value;

            var pattern = @"answers\[(\d+)\]";

            Match match = Regex.Match(value, pattern);

            if (match.Success)
                return Convert.ToInt64(match.Groups[1].Value);

            return -1;
        }

        private IEnumerable<String> GetMultiQuestions(HtmlNodeCollection html)
        {
            foreach (var question in html)
            {
                var output = Regex.Replace(question.InnerHtml, @"<select [\w\W]*>[\w\W]*</select>", String.Empty);

                yield return output.Trim();
            }
        }

        private void ChangeAnswersListTypeToNumber(HashSet<String> answers)
        {
            var data = new HashSet<String>();

            Byte i = 1;

            foreach (var item in answers)
            {
                Char[] array = item.ToCharArray();

                array[0] = Convert.ToChar(i.ToString());

                i++;

                data.Add(new String(array));
            }

            answers.Clear();

            foreach (var newAnsw in data)
            {
                answers.Add(newAnsw);
            }
        }

        #endregion

    }
}
