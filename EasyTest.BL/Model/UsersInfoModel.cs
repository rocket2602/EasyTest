namespace EasyTest.BL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UsersInfo")]
    public  class UsersInfo
    {
        public UsersInfo()
        {
            QuizQuestions = new HashSet<QuizQuestionModel>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }

        [Required]
        [StringLength(255)]
        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        public string MidleName { get; set; }

        public int CountryId { get; set; }

        public int UniversityId { get; set; }

        public int DepartamentId { get; set; }

        public int CathedraId { get; set; }

        public int Course { get; set; }

        [StringLength(10)]
        public string Groups { get; set; }

        public DateTime Datetime { get; set; }

        public virtual ICollection<QuizQuestionModel> QuizQuestions { get; set; }
    }
}
