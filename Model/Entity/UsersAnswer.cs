namespace Model.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UsersAnswer
    {
        public long Id { get; set; }

        public int QuiztId { get; set; }

        public long QuestionId { get; set; }

        public long? MQuestionId { get; set; }

        public long AnswerId { get; set; }

        public int UserId { get; set; }

        public byte? IsRight { get; set; }

        public string OpenAnswer { get; set; }

        public int? Numbers { get; set; }

        public long TimeCreated { get; set; }

        public virtual QuizAnswer QuizAnswer { get; set; }

        public virtual QuizMultiQuestion QuizMultiQuestion { get; set; }

        public virtual QuizQuestion QuizQuestion { get; set; }

        public virtual UsersInfo UsersInfo { get; set; }
    }
}
