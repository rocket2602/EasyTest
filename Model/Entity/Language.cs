namespace Model.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Language
    {
        public Language()
        {
            QuestionsTypeLangs = new HashSet<QuestionsTypeLang>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(12)]
        public string Code { get; set; }

        [Required]
        [StringLength(255)]
        public string NativeName { get; set; }

        [Required]
        [StringLength(255)]
        public string EnglishName { get; set; }

        [Required]
        [StringLength(3)]
        public string Direction { get; set; }

        [Required]
        [StringLength(5)]
        public string Icon { get; set; }

        public virtual ICollection<QuestionsTypeLang> QuestionsTypeLangs { get; set; }
    }
}
