﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyTest.BL.Quiz.Model
{
    public abstract  class QuizQuestionModel
    {
        public QuizQuestionModel()
        {
            Answers = new HashSet<String>();

            MultiQuestions = new HashSet<String>();         
        }

        public String Question { get; set; }

        public HashSet<String> Answers { get; set; }

        public HashSet<String> MultiQuestions { get; set; }

    }
}
