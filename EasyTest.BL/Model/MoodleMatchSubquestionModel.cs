namespace EasyTest.BL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public  class MoodleMatchSubquestionModel
    {
        public int Id { get; set; }

        public long QuestionId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Answer { get; set; }

        public virtual QuizQuestionModel QuizQuestion { get; set; }
    }
}
