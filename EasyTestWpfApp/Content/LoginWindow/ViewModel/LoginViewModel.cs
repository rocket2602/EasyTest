﻿using EasyTestWpfApp.Helpers;
using EasyTestWpfApp.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Windows;
using System.ComponentModel;

namespace EasyTestWpfApp.Content.LoginWindow.ViewModel
{
    public  class LoginViewModel : ViewModelBase, IDataErrorInfo
    {
        private String _Username, _Password;

        public LoginViewModel()
        {
            SignIn = new RelayCommand(SignInUser,CanUserSignIn);
            GoToUrl = new RelayCommand<String>(OpenBrowserPage);
            Exit = new RelayCommand<Window>(CloseWindow);
        }

        #region Properties


        public String UserName
        {
            get { return _Username; }
            set
            {
                _Username = value;
                RaisePropertyChanged("UserName");
            }
        }

        public String Password
        {
            get { return _Password; }
            set { _Password = value; RaisePropertyChanged("Password"); }
        }

        public String ResetPassword
        {
            get { return Settings.GetAppSettingsByKey("ResetPasswordLink"); }
        }

        public String Registration
        {
            get { return Settings.GetAppSettingsByKey("RegistrationLink"); }
        }

        public String FacebookLink
        {
            get { return Settings.GetAppSettingsByKey("FacebookPage"); }
        }
        public String GoogleLink
        {
            get { return Settings.GetAppSettingsByKey("GooglePage"); }
        }
        public String TwitterLink
        {
            get { return Settings.GetAppSettingsByKey("TwitterPage"); }
        }

        #endregion

        #region Command
        public RelayCommand SignIn { get; private set; }
        public RelayCommand<String> GoToUrl { get; private set; }
        public RelayCommand<Window> Exit { get; private set; }

        public string Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string this[string columnName]
        {
           get
            {
                var result = String.Empty;
                switch (columnName)
                {

                    case "UserName": if (!Validation.IsEmail(UserName)) result = "todo"; break;

                };
                return result;
            }
        }
        #endregion

        #region Method


        private bool CanUserSignIn()
        {
            if (Validation.Username(_Username) && Validation.Password(_Password)) return true;

            return false;
        }

        private async void SignInUser()
        {
            var token = await HttpHelper.SignInUser("localhost:[port]",UserName,Password);

            if (token == null)
            {
                //LoginError = "[Невірний email чи пароль]";       
                return;
            }

            //if (UsernameIsNotContainse())
            //{
            //    _emails.Add(UserName);
            //    settings.SaveUserName(UserName);
            //}

            Messenger.Default.Send<Token>(token);

            Password = null;
        }

        private void CloseWindow(Window obj)
        {
            obj.Close();
        }

        private void OpenBrowserPage(String link)
        {
            Process.Start(new ProcessStartInfo(link));
        }
        #endregion
    }
}
