﻿using Fiddler;
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;
using System.Net.Http;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using EasyTest.BL.Helpers;
using EasyTest.BL.Quiz.Model;
using EasyTest.BL.Entity;


namespace EasyTest.BL.Quiz
{
    public class AtutorQuiz : Quiz
    {
        private const String oneQuestionPerPagePattern = @"^\/atutor\/mods\/_standard\/tests\/take_test_q\.php\?tid=\d+$";

        public AtutorQuiz()
        {
            StartTestUrl = "/atutor/mods/_standard/tests/test_intro.php";
            TestPagePattern = @"^\/atutor\/mods\/_standard\/tests\/take_test\.php\?tid=\d+$";
            FinishTestUrl = "/atutor/mods/_standard/tests/take_test.php";
        }



        private QuizStatus _status;

        public override QuizStatus Status
        {
            get { return _status; }

            set { _status = value; StatusChanged(); }
        }

        public override event Action StatusChanged;

        protected override async void FiddlerApplication_AfterSessionComplete(Session oSession)
        {
            base.CheckFiddlerSettings(oSession);

            String PatchAndQuery = oSession.PathAndQuery;


          //  quiztData.Cookie = oSession.oRequest.headers["Cookie"];


          //  Debug.WriteLine(quiztData.Cookie);

            //Start quiz
            if (String.Compare(PatchAndQuery, StartTestUrl) == 0 && oSession.oRequest.headers.HTTPMethod == "POST")
            {
                Status = QuizStatus.Start;

                quiztData.Cookie = oSession.oRequest.headers["Cookie"];

                String UserAgent = oSession.oRequest.headers["User-Agent"];

                quiztData.parameter.Add("UserAgent", UserAgent);

                var reqBody = Encoding.UTF8.GetString(oSession.RequestBody);

                var QueryParams = HttpUtility.ParseQueryString(reqBody);

                quiztData.tid = Convert.ToInt32(QueryParams["tid"]);

                return;
            }


            //Get quiz questions all in page
            if (Regex.IsMatch(PatchAndQuery, TestPagePattern, RegexOptions.IgnoreCase))
            {
                if (Status != QuizStatus.Start)
                {
                    throw new Exception("Test is started before starting scan!!!");
                }

                Status = QuizStatus.ToDo;

                quiztData.nextPage = -1;//all questions in page

                GetQuestionsFromHtmlQuiz(oSession.GetResponseBodyAsString());

                await ChangeIframeIfExistsToHtml();

                Status = QuizStatus.QuestionsInside;

                return;
            }

            //one question per page
            if (Regex.IsMatch(PatchAndQuery, oneQuestionPerPagePattern, RegexOptions.IgnoreCase))
            {
                if (Status != QuizStatus.Start)
                {
                    throw new Exception("Test is started before starting scan!!!");
                }

                Status = QuizStatus.ToDo;

                quiztData.nextPage = 1;

                String text = oSession.GetResponseBodyAsString();

                GetQuestionsFromHtmlQuiz(text);


                var lastPage = GetNumberOfLastPage(text);

                quiztData.parameter.Add("LastPage", lastPage.ToString());

                await GetNextPageOfTest();

                await ChangeIframeIfExistsToHtml();

                Status = QuizStatus.QuestionsInside;

                return;
            }

            if (String.Compare(PatchAndQuery, FinishTestUrl) == 0 && oSession.oRequest.headers.HTTPMethod == "POST")
            {
                Status = QuizStatus.Finish;

                return;
            }
        }

        private Int32 GetNumberOfLastPage(string text)
        {
            var htmlDoc = new HtmlDocument();

            htmlDoc.LoadHtml(text);

            String name;

            var elem = htmlDoc.DocumentNode.SelectSingleNode(TestNameXPath);

            name = elem != null ? elem.InnerText : String.Empty;

            var pattern = @"\d+\/(\d+)";

            Match match = Regex.Match(name, pattern);

            if (match.Success)
            {
                return Convert.ToInt32(match.Groups[1].Value) - 1;
            }

            return -1;

        }

        private const String TestNameXPath = "//legend[@class='group_form']";

        private const String CourseNameXPath = "//h1[@id='section-title']";


        protected override void GetQuestionsFromHtmlQuiz(String text)
        {
            if (String.IsNullOrEmpty(text))
            {
                throw new ArgumentException();
            }

            var htmlDoc = new HtmlDocument();

            htmlDoc.LoadHtml(text);

            quiztData.quizName = GetQuiztName(htmlDoc, CourseNameXPath, TestNameXPath);

            var questions = htmlDoc.DocumentNode.SelectNodes("//div[@class='row']");

            if (questions != null)
            {
                foreach (var item in questions)
                {
                    html.AppendLine(item.OuterHtml);
                }
            }
        }

        private async Task GetNextPageOfTest()
        {
            using (var handler = new HttpClientHandler { UseCookies = false })
            using (var client = new HttpClient(handler) { BaseAddress = new Uri(domain) })
            {
                var url = String.Format("/atutor/mods/_standard/tests/take_test_q.php?tid={1}&pos={0}", quiztData.nextPage, quiztData.tid);

                var message = new HttpRequestMessage(HttpMethod.Get, url);

                message.Headers.Add("Cookie", quiztData.Cookie);
                message.Headers.Add("User-Agent", quiztData.parameter["UserAgent"]);

                var response = await client.SendAsync(message);

                if (response.IsSuccessStatusCode)
                {
                    var htmlDoc = new HtmlDocument();

                    String ResponceHtml = await response.Content.ReadAsStringAsync();

                    htmlDoc.LoadHtml(ResponceHtml);

                    var question = htmlDoc.DocumentNode.SelectSingleNode("//div[@class='row']");

                    if (question != null)
                    {
                        html.AppendLine(question.OuterHtml);

                        quiztData.nextPage++;
                    }
                    else
                    {
                        quiztData.nextPage = -1;
                    }



                    if (quiztData.nextPage != -1)
                    {
                        if (quiztData.nextPage <= Convert.ToInt32(quiztData.parameter["LastPage"]))
                            await GetNextPageOfTest();
                    }

                }
            }
        }

        //+unit test todo
        private async Task ChangeIframeIfExistsToHtml()
        {
            var htmlDoc = new HtmlDocument();

            htmlDoc.LoadHtml(html.ToString());

            var iframes = htmlDoc.DocumentNode.SelectNodes("//iframe");

            if (iframes == null) return;

            foreach (var item in iframes)
            {
                String src = item.Attributes["src"].Value;

                HtmlNode elem = htmlDoc.CreateElement("div");

                String iframeHtml = await HttpHelper.GetHtmlFromIframeAsync(src, quiztData);

                elem.InnerHtml = iframeHtml;

                item.ParentNode.ReplaceChild(elem, item);
            }

            html.Clear();

            html.Append(htmlDoc.DocumentNode.InnerHtml);
        }


        private List<AtutorQuizQuestion> questions;

        public override void ParseQuiz()
        {
            Status = QuizStatus.ParseStart;

            var parser = AtutorQuizParser.Instance;

            questions = parser.ParseHtml(html.ToString());

            Status = QuizStatus.ParseComplated;
        }

        private Int32 tid;

        public async override void SaveUniqueQuestions()
        {
            Status = QuizStatus.SaveUniqueStart;

            var qid = questions.Select(x => x.qid).Distinct().ToArray();

            long[] unique = await HttpHelper.GetUniqueqidAsync(qid);//errors todo

            var onlyUniqueQuestions = questions.Where(x => unique.Contains(x.qid)).ToList();

            var data = new
            {
                tid = quiztData.tid,
                quizName = quiztData.quizName,
                questions = onlyUniqueQuestions
            };

            tid = await HttpHelper.SaveUniqueQuestionsAsync(data, "api/AtutorQuiz/SaveUnique");//error todo

            Status = QuizStatus.SaveUniqueComplated;
        }



        public override async void GetQuizWithAnswers()
        {
            Status = QuizStatus.GetAnswersStart;

            var qids = questions.Select(x => x.qid).ToArray();

            Result = await HttpHelper.GetQuestionsAsync("api/AtutorQuiz/getanswers", tid, qids);

            Status = QuizStatus.GetAnswersComplated;
        }

    }
}
