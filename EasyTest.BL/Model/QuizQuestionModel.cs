namespace EasyTest.BL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public  class QuizQuestionModel
    {
        public QuizQuestionModel()
        {
            MoodleMatchSubquestions = new HashSet<MoodleMatchSubquestionModel>();
            QuizAnswers = new HashSet<QuizAnswerModel>();
            QuizMultiQuestions = new HashSet<QuizMultiQuestionModel>();
        }

        public long Id { get; set; }

        public int QuizId { get; set; }

        public Int16 Type { get; set; }

        [Required]
        public string Name { get; set; }

        public int? UserId { get; set; }

        public long? ParentId { get; set; }

        public long TimeCreated { get; set; }

        public virtual ICollection<MoodleMatchSubquestionModel> MoodleMatchSubquestions { get; set; }

        public virtual QuestionsTypeModel QuestionsType { get; set; }

        public virtual ICollection<QuizAnswerModel> QuizAnswers { get; set; }

        public virtual ICollection<QuizMultiQuestionModel> QuizMultiQuestions { get; set; }

        public virtual UsersInfo UsersInfo { get; set; }
    }
}
