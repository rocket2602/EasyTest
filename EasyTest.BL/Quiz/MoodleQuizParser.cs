﻿using EasyTest.BL.Quiz.Model;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace EasyTest.BL.Quiz
{
    public class MoodleQuizParser
    {
        private readonly List<MoodleQuizQuestion> _questions;

        private MoodleQuizQuestion _quizStorage;

        public MoodleQuizParser()
        {
            _questions = new List<MoodleQuizQuestion>();
        }

        public  void ParseHtml(String html)
        {
            HtmlNode.ElementsFlags.Remove("option");

            var htmlDoc = new HtmlDocument();

            htmlDoc.LoadHtml(html);

            var questions = htmlDoc.DocumentNode
                                   .Descendants("div")
                                   .Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value
                                                                                                      .Split(' ')
                                                                                                      .Any(b => b.Equals("que")));

            if (questions == null)
            {
                //add my exaption todo
                throw new Exception("aa");
            }

            foreach (var item in questions)
            {

                _quizStorage = new MoodleQuizQuestion();



                var qtype = item.Attributes["class"].Value.Split(' ');

                switch (qtype[1])
                {
                    case "truefalse":

                        _quizStorage.Type = MoodleQuestionType.truefalse;
                        GetQuestion(item, ".//div[@class='qtext']");
                        GetAnswers(item, ".//div[@class='answer']//label");

                        break;

                    case "multichoice":

                        GetQuestion(item, ".//div[@class='qtext']");
                        _quizStorage.Type = GetMultiAnswerType(item);
                        GetAnswers(item, ".//div[@class='answer']//label");

                        break;

                    case "essay":
                    case "shortanswer":
                    case "numerical":

                        _quizStorage.Type = (MoodleQuestionType)System.Enum.Parse(typeof(MoodleQuestionType), qtype[1]);
                        GetQuestion(item, ".//div[@class='qtext']");

                        break;

                    case "match":
                    case "randomsamatch":

                        _quizStorage.Type = (MoodleQuestionType)Enum.Parse(typeof(MoodleQuestionType), qtype[1]);
                        GetQuestion(item, ".//div[@class='qtext']");
                        GetMatchSubquestions(item);

                        break;

                    case "calculated":

                        _quizStorage.Type = MoodleQuestionType.calculated;
                        GetQuestion(item, ".//div[@class='qtext']");
                        ReplaceNumbersFromQuetionToVariable();

                        break;

                    case "calculatedmulti":

                        _quizStorage.Type = MoodleQuestionType.calculatedmulti;
                        GetQuestion(item, ".//div[@class='qtext']");
                        ReplaceNumbersFromQuetionToVariable();
                        _quizStorage.AnswersType = GetMultiAnswerType(item);
                        GetAnswers(item, ".//div[@class='answer']//label");

                        //for (int i = 0; i < _quizStorage.Answers.Count; i++)
                        //{
                        //    _quizStorage.Answers[i] = Regex.Replace(_quizStorage.Answers[i], @"\b(\d+\.\d+|\d+)", ReplaceVariableToNumberInCalculatedQuestion, RegexOptions.Multiline);
                        //}

                        break;

                    case "calculatedsimple":

                        _quizStorage.Type = MoodleQuestionType.calculatedsimple;
                        GetQuestion(item, ".//div[@class='qtext']");
                        ReplaceNumbersFromQuetionToVariable();

                        break;

                    case "multianswer":

                        _quizStorage.Type = MoodleQuestionType.multisubanswers;
                        _quizStorage.Question = ParseEmbeddedQuestion(item);

                        break;

                    default:

                        throw new Exception("not found question");
                }

                _questions.Add(_quizStorage);
            }
        }

        private String ParseEmbeddedQuestion(HtmlNode item)
        {
            var elem = item.SelectSingleNode(".//div[@class='formulation']");

            if (elem == null) return "Cannot get question from embeded type";

            var subquestions = elem.SelectNodes(".//span[@class='subquestion']");

            ParseEmbeddedSubQuestions(subquestions);

            var subMultiChoice = elem.SelectNodes(".//*[@class='answer']");

            ParseEmbeddedMultiChoiceSubQuestions(subMultiChoice);

            var temp = elem.SelectSingleNode(".//h4[@class='accesshide']");

            if (temp != null) temp.Remove();

            return elem.InnerText;

        }

        private void ParseEmbeddedMultiChoiceSubQuestions(HtmlNodeCollection subMultiChoice)
        {
            if (subMultiChoice == null)
            {
                //exaption here todo
                return;
            }

            foreach (var subqMulti in subMultiChoice)
            {
                var labels = subqMulti.SelectNodes(".//label");

                var str = new StringBuilder();

                var type = subqMulti.OriginalName == "div" ? "{MCV:" : "{MCH:";

                str.Append(type);

                labels.ToList().ForEach(x => str.Append("~").Append(x.InnerText));

                str.Append("}");

                ReplaceEmbeddedSubquestionToText(subqMulti, str.ToString());
            }
        }

        private void ReplaceEmbeddedSubquestionToText(HtmlNode node, String text)
        {
            var nodeText = new HtmlDocument().CreateTextNode(text);

            node.ParentNode.ReplaceChild(nodeText, node);
        }

        private void ParseEmbeddedSubQuestions(HtmlNodeCollection subquestions)
        {
            if (subquestions == null)
            {
                //exaption here todo
                return;
            }


            foreach (var subq in subquestions)
            {
                var input = subq.SelectSingleNode(".//input");

                if (input != null)//input
                {
                    ReplaceEmbeddedSubquestionToText(subq, "{SA:}");

                    continue;
                }

                var options = subq.SelectNodes(".//option").Skip(1);

                var str = new StringBuilder();

                str.Append("{MC:");

                options.ToList().ForEach(x => str.Append("~").Append(x.InnerText));

                str.Append("}");

                ReplaceEmbeddedSubquestionToText(subq, str.ToString());
            }
        }



        private String ReplaceVariableToNumberInCalculatedQuestion(Match match)
        {
            String key = _quizStorage.CalculatedValues.FirstOrDefault(x => x.Value.ToString() == match.Value).Key;

            if (key == null) return match.Value;

            return key;
        }

        private MoodleQuestionType GetMultiAnswerType(HtmlNode item)
        {
            var multiType = item.SelectNodes(".//input[not(@type='hidden')]")
                                       .First()
                                       .Attributes["type"]
                                       .Value;




            if (String.Compare(multiType, "radio") == 0)
                return MoodleQuestionType.multichoice;

            return MoodleQuestionType.multianswer;
        }

        private void ReplaceNumbersFromQuetionToVariable()
        {
            i = 0;
            _quizStorage.Question = Regex.Replace(_quizStorage.Question, @"\b(\d+\.\d+|\d+)", ReplaceNumberToVarialeInCalculatedMultiAnswer, RegexOptions.Multiline);
        }

        Int32 i;
        private String ReplaceNumberToVarialeInCalculatedMultiAnswer(Match match)
        {
            Double val = Convert.ToDouble(match.Value);

            String key = String.Format("#x{0}", ++i);

            _quizStorage.CalculatedValues.Add(key, val);

            return key;
        }

        private void GetMatchSubquestions(HtmlNode item)
        {
            var mquestions = item.SelectNodes(".//td[@class='text']");

            if (mquestions == null) return;

            foreach (var mq in mquestions)
            {
                _quizStorage.MultiQuestions.Add(mq.InnerHtml);
            }

            var answers = item.SelectNodes(".//td[@class='control']//select").FirstOrDefault();

            if (answers == null) return;



            var manswers = answers.SelectNodes(".//option[not(@value=0)]");

         

            foreach (var ma in manswers)
            {
                _quizStorage.Answers.Add(ma.InnerText);
            }
        }

        private void GetAnswers(HtmlNode item, string xPath)
        {
            var answers = item.SelectNodes(xPath);

            if (answers == null) return;

            foreach (var elem in answers)
            {
                _quizStorage.Answers.Add(elem.InnerHtml);
            }
        }

        private void GetQuestion(HtmlNode item, string xPath)
        {
            var question = item.SelectSingleNode(xPath);

            if (question == null) return;

            _quizStorage.Question = question.InnerHtml;
        }

        public   List<MoodleQuizQuestion> GetResult() 
        {
            return _questions;
        }
    }
}
