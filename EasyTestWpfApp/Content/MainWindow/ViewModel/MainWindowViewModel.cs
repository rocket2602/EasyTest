﻿using EasyTestWpfApp.Content.MainWindow.View.Pages;
using EasyTestWpfApp.Helpers;
using EasyTestWpfApp.Model;
using Fiddler;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;

namespace EasyTestWpfApp.Content.MainWindow.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            GetPage1 = new RelayCommand(Get);


          

        }

     

        public RelayCommand GetPage1 { get; private set; }

        private readonly Token _token;

        //public MainWindowViewModel(Token token)
        //{
        //    _token = token;
        //}

        private void Get()
        {
            Switcher.Switch(new MainMenuView());
        }
    }
}
