﻿using EasyTest.BL.Entity;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace EasyTestWpfApp
{
    public class QuizTemplateSelector : DataTemplateSelector
    {
        #region QuestionTypeTemplate
        public DataTemplate MultiChoiceTemplate { get; set; }

        public DataTemplate MultiAnswerTemplate { get; set; }


        public DataTemplate OrderingTemplate { get; set; }

        public DataTemplate OpenEnderTemplate { get; set; }

        public DataTemplate MatchingTemplate { get; set; }

        public DataTemplate EssayTemplate { get; set; }

        public DataTemplate NumericalTemplate { get; set; }

        public DataTemplate MatchTemplate { get; set; }

        public DataTemplate RandomsamatchTemplate { get; set; }

        public DataTemplate CalculatedTemplate { get; set; }
        #endregion
  
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item != null && item is QuizQuestionModel)
            {
                var question = item as QuizQuestionModel;

                switch (question.QuestionsType.Name)
                {
                    case "truefalse":
                    case "multichoice":
                        return MultiChoiceTemplate;
                    case "multianswer": return MultiAnswerTemplate;
                    case "ordering":return OrderingTemplate;
                    case "openEnder":return OpenEnderTemplate;
                    case "matchingSimple":
                    case "matchingGraphical":
                        return MatchingTemplate;

                    default: return null;
                }
            }

            return null;
        }
    }
}
